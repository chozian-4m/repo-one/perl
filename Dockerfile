ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=latest

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY perl.tar.gz /

RUN dnf update -y \
    && dnf install make gcc gcc-c++ -y \
    && mkdir /usr/local/src/perl \
    && tar -xzf perl.tar.gz --strip-components=1 -C /usr/local/src/perl \
    && cd /usr/local/src/perl \
    && ./Configure -de \
    && make \
    && make test -i \ 
    && make install \
    && dnf remove gcc make -y \
    && dnf clean all \
    && cd / \
    && rm -rf /var/cache/dnf/ /var/tmp/* /perl.tar.gz /usr/local/src/perl /tmp/*

WORKDIR /opt/app-root/src
RUN useradd -u 1001 -g 0 -M -d /opt/app-root/src default && \
    chown -R 1001:0 /opt/app-root/src

USER 1001

CMD ["perl","-de0"]
