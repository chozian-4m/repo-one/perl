# Perl
 
Perl is a high-level, general-purpose, interpreted, dynamic programming language. The Perl language borrows features from other programming languages, including C, shell scripting (sh), AWK, and sed.


This Perl container is based off the following:
 - https://hub.docker.com/_/perl
 - https://github.com/Perl/docker-perl

# Creating a Dockerfile in your Perl project to run the app
To run your app inside the Perl container, add something along the lines of the following to a Dockerfile within your project, and it will compile and run your app:
```
FROM perl:<version>

COPY . /usr/src/myapp
WORKDIR /opt/app-root/src

CMD [ "perl", "./your-daemon-or-script.pl"]
```
# Modules
Perl modules provide a range of features to help you avoid reinventing the wheel, and can be downloaded from [CPAN](http://www.cpan.org/). A number of popular modules are included with the Perl distribution itself([here](https://perldoc.perl.org/perlmodlib.html) is the comprehensive list).

For help installing the modules, click [here](https://perldoc.perl.org/perlmodinstall.html).
# Documentation

The documentation for Perl can be found [here](https://perldoc.perl.org/index.html).
